import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'listScreen.dart' as lists;

class Connection extends StatelessWidget {

  /// Modifier la valeur de l'IP pour pointer sur la bonne machine
  final ip = "192.168.1.89";

  connection(String email, String password, BuildContext context) async{
      final http.Response response = await http.post(
          "http://" + ip + ":81/api/login_check",
          headers: <String, String>{
            'Content-Type':'application/json; charset=UTF-8',
            'Accept-Encoding':'gzip, deflate, br',
            'Connection':'keep-alive'
          },
          body: jsonEncode(<String, String>{
            "username":email,
            "password":password
          })
      );

      if(response.statusCode == 204){
        Navigator.push(
            context,
          MaterialPageRoute(builder: (context) => lists.ListScreen())
        );
      }else{
        throw Exception ("Vous n'êtes pas connecté, code :"+ response.statusCode.toString());
      }
  }

  @override
  Widget build(BuildContext context) {
    return Container(

    );
  }
}
