import 'package:flutter/material.dart';

class User{
  final int id;
  final String firtName;
  final String lastName;
  final String email;
  bool isUserSession;

  User({this.id, this.firtName, this.lastName, this.email});

  factory User.fromJson(Map<String, dynamic> json){
    return User(
      id: json['id'],
      firtName: json['firstName'],
      lastName: json['lastName'],
      email: json['email']
    );
  }

  /*

  Peut être utile !!
  int getId(){
    return this.id;
  }

  String getFirstName(){
    return this.firtName;
  }

  String getLasttName(){
    return this.lastName;
  }

  String getEmail(){
    return this.email;
  }

  bool getIsUserSession(){
    return this.getIsUserSession();
  }
*/
}