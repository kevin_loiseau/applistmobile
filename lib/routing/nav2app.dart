
import 'package:app_list_demo/connectionform.dart';
import 'package:app_list_demo/listScreen.dart';
import 'package:flutter/material.dart';


class Nav2App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/': (context) => ConnectionForm(),
        '/listes': (context) => ListScreen(),
      },
    );
  }
}