import 'package:flutter/material.dart';
import 'connection.dart' as connect;


// ignore: must_be_immutable
class ConnectionForm extends StatelessWidget {

  String email="";
  String password="";
  final _keyForm = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("AppList Connexion"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(

          padding: EdgeInsets.symmetric(
              vertical: 200.0,
              horizontal: 30.0
          ),
          child: Form(
            key: _keyForm,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                TextFormField(
                  decoration: InputDecoration(
                    labelText: "Votre email",
                    border: OutlineInputBorder()
                  ),
                  validator: (val) => val.isEmpty ? "Veuillez saisir votre email" : null,
                  onChanged: (val) => email = val,
                ),
                SizedBox(height: 10.0),
                TextFormField(
                  decoration: InputDecoration(
                      labelText: "Votre mot de passe",
                      border: OutlineInputBorder()
                  ),
                  obscureText: true,
                  validator: (val) => val.isEmpty ? "Veuillez saisir votre mot de passe" : null,
                  onChanged: (val) => password = val,
                ),
                SizedBox(height: 10.0),
                OutlineButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  onPressed: (){
                    if(_keyForm.currentState.validate()){
                      connect.Connection connection = new connect.Connection();
                      connection.connection(email, password, context);
                    }
                  },
                  borderSide: BorderSide(
                    width: 1.0,
                    color: Colors.green
                  ),
                  child: Text(
                      "connexion",
                    style: TextStyle(
                      color: Colors.green
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
