
import 'package:flutter/material.dart';
import 'connectionform.dart';

void main()=>runApp(AppList());
class AppList extends StatelessWidget{


  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.lightBlue,
      ),
      home: ConnectionForm(),
    );
  }
}